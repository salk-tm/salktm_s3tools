import subprocess
import pandas
import argparse


def rename_org_args():
    parser = argparse.ArgumentParser(
        description=(
            "renames a set of files associated with an organism ID, updating prefixes as required"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "oldid",
        help="an old organism id to be replaced"
    )
    parser.add_argument(
        "newid",
        help="a new orgnaism ID"
    )
    parser.add_argument(
        "-r", "--reads_dir",
        default="s3://salk-tm-raw/reads/",
        help="s3 location to locate for organism directories"
    )
    parser.add_argument(
        "-d", "--dryrun",
        default=True,
        help="if true, do not execute renamings, only show which renamings would occur"
    )
    return parser.parse_args()


def get_paths(uri):
    result = subprocess.run(["aws", "s3", "ls", uri, "--recursive"], stdout=subprocess.PIPE)


def rename_org():
    pass

