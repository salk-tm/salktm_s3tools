PACBIO_METADATA_SUFFIXES = ("hifi_reads.5mc_report.json", "baz2bam.log", "ccs.log",
                            "ccs_report.json", "ccs_report.txt",
                            "hifi_reads.consensusreadset.xml", "hifi_reads.primrose.log", "sts.xml",
                            "transferdone", "metadata.xml", "barcodes.fasta","hifi_reads.lima_summary.txt")