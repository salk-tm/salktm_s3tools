import pandas
import argparse
import os
import os.path
import sys
import glob

SHEET_ID = "1-r0PweCt4K0hQsKrc1uxjkNAZMee-PwJxkYVxPGprig"
SHEET_NAME = "RUNS"

run_contents = ["fastq_pass", "fastq_fail", "fast5_pass", "fast5_fail"]
pod5_run_contents = ["fastq_pass", "fastq_fail", "pod5_pass", "pod5_fail"]
new_pod5 = ["fastq_pass", "fastq_fail", "pod5"]

def search_rundir(f, run_contents=run_contents, pod5_run_contents=pod5_run_contents):
    children = glob.glob(f"{f}/*")
    if(all(f"{f}/{d}" in children for d in run_contents) or 
       all(f"{f}/{d}" in children for d in pod5_run_contents) or
       all(f"{f}/{d}" in children for d in new_pod5)):
        return [f]
    else:
        return [d for c in children for d in search_rundir(c)]

def load_sheet(run_id, id=SHEET_ID, name=SHEET_NAME):
    """ given a sheet id and a tab name, return the data found in that
        spreadsheet tab as a pandas dataframe
    """
    url = f"https://docs.google.com/spreadsheets/d/{id}/gviz/tq?tqx=out:csv&sheet={name}"
    df = pandas.read_csv(url)
    upload_stat = (df.loc[df['RUN_ID'] == run_id]).UPLOADED
    return upload_stat.to_string(index=False)

parser = argparse.ArgumentParser(description="prints a quick report on some number of run directories and shows upload status")
parser.add_argument("rundir", nargs="+")
args = parser.parse_args()

def test_rundir():
    df = pandas.DataFrame([{'RUNDIR': d, "RUNPATH": p} for d in args.rundir for p in search_rundir(d)])
    df['FLOWCELL'] = df.RUNPATH.str.split("/").str[-1].str.split("_").str[-2]
    df['IS_FINISHED'] = df.RUNPATH.apply(lambda d: len(glob.glob(f"{d}/final*")) > 0)
    df['IS_UPLOADED'] = [load_sheet(x) for x in df['RUNDIR'].tolist()]
    df.to_csv(sys.stdout, sep="\t", index=None)