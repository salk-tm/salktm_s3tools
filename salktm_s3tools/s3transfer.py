from subprocess import Popen, PIPE, call
import pandas
import argparse
import os
import os.path
import glob
import sys

from salktm_s3tools.parse_xml import match_library_context
from salktm_s3tools.pacbio_metadata import PACBIO_METADATA_SUFFIXES

SHEET_ID = "1-r0PweCt4K0hQsKrc1uxjkNAZMee-PwJxkYVxPGprig"
SHEET_NAME = "RUNSxLIBRARIES"
S3PREFIX = "s3://salk-tm-raw/reads/"


ONT_grid_IDX = {
    f"{c}{i+1}": f"RB{i * 8 + r + 1:02d}"
    for r, c in enumerate("ABCDEFGH")
    for i in range(0, 12)
}


def load_sheet(id=SHEET_ID, name=SHEET_NAME):
    """given a sheet id and a tab name, return the data found in that
    spreadsheet tab as a pandas dataframe
    """
    url = (
        f"https://docs.google.com/spreadsheets/d/{id}/gviz/tq?tqx=out:csv&sheet={name}"
    )
    df = pandas.read_csv(url)
    return df.dropna(how="all").dropna(axis=1, how="all")


def load_sample_sheet(sheet_id, sheet_name):
    samples_df = load_sheet(sheet_id, sheet_name)
    if samples_df.SHORT_LABEL.str.contains("\s").any():
        bad_rows = samples_df[samples_df.SHORT_LABEL.str.contains("\s").fillna(False)]
        bad_rows.to_csv(sys.stderr, sep="\t", index=None)
        raise ValueError("SHORT_LABEL must not contain white spaces. Bad rows above")
    samples_df["LABEL"] = [
        f"{dt}" if sl == "" else f"{dt}_{sl}"
        for dt, sl in zip(samples_df.DTYPE, samples_df.SHORT_LABEL.fillna(""))
    ]
    return samples_df


def s3ls(s3pre=S3PREFIX):
    """performs a recursive ls for a given prefix and returns a dataframe
    representing the results
    """
    cmd = f"aws s3 ls {s3pre} --recursive"
    with Popen(cmd, stdout=PIPE, stderr=None, shell=True) as process:
        output = process.communicate()[0].decode("utf-8").rstrip()
    ret = pandas.DataFrame(
        [line.split() for line in output.split("\n")],
        columns=["DATE", "TIME", "SIZE", "PATH"],
    )
    ret.SIZE = ret.SIZE.astype(int)
    ret["STAMP"] = (ret.DATE + " " + ret.TIME).apply(
        lambda s: pandas.Timestamp(s).timestamp()
    )
    return ret


def s3uri_test(s):
    if s.startswith("s3://") and s.endswith("/"):
        return s
    else:
        raise ValueError("s3 prefixes must start with 's3://' and end with '/' ")


def s3transfer_cli():
    parser = argparse.ArgumentParser(
        description=("transfers a set of read files from local to s3"),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("rundir", help="a run directory to transfer")
    parser.add_argument(
        "-r",
        "--runid",
        help="The run id matching your run. Defaults to 'rundir' ",
        default=None,
    )
    parser.add_argument(
        "-p",
        "--platform",
        choices=["ont", "ill", "pb", "kinnex"],
        default="ont",
        help="which sequencer we are running on",
    )
    parser.add_argument(
        "-s",
        "--s3prefix",
        default="s3://salk-tm-raw/reads/",
        type=s3uri_test,
        help="s3 location to transfer reads to",
    )
    parser.add_argument(
        "-d",
        "--dryrun",
        action="store_true",
        help="if true, do not execute transfer, only show which would occur",
    )
    parser.add_argument(
        "-i",
        "--sheet_id",
        help="the sheet id containing runs and libraries",
        default=SHEET_ID,
    ),
    parser.add_argument(
        "-n",
        "--sheet_name",
        help="the name of the sheet containing runs and libraries",
        default=SHEET_NAME,
    )
    parser.add_argument(
        "-w",
        "--wkdir",
        help="A location where a working directory will be created (and deleted upon exit 0)",
        default="./s3_transfer_temp",
    )
    parser.add_argument(
        "--omit-library",
        nargs="+",
        default=[],
        help=(
            "use this to omit one or more libraries from upload, so that a"
            "problem with one library does not make it difficult to upload"
            "an entire run. Hopefully you never have to use this"
        ),
    )
    args = parser.parse_args()
    if args.runid is None:
        args.runid = args.rundir
    s3uri_test(args.s3prefix)
    if args.platform == "ont":
        transfer_ont(
            args.rundir,
            args.runid,
            args.s3prefix,
            args.dryrun,
            args.sheet_id,
            args.sheet_name,
            args.wkdir,
        )
    elif args.platform == "ill":
        transfer_ill(
            args.rundir,
            args.runid,
            args.s3prefix,
            args.dryrun,
            args.sheet_id,
            args.sheet_name,
            args.wkdir,
            omitted_libraries=args.omit_library,
        )
    elif args.platform == "pb":
        transfer_pb(
            args.rundir,
            args.runid,
            args.s3prefix,
            args.dryrun,
            args.sheet_id,
            args.sheet_name,
            args.wkdir,
            omitted_libraries=args.omit_library,
        )
    elif args.platform == "kinnex":
        transfer_kinnex(
            args.rundir,
            args.runid,
            args.s3prefix,
            args.dryrun,
            args.sheet_id,
            args.sheet_name,
            args.wkdir,
            omitted_libraries=args.omit_library,
        )


def test_state(samples_df, s3ls_df):
    cols = ["ORGANISM_ID", "DTYPE", "LIBRARY_ID", "RUN_ID", "SHORT_LABEL"]
    for c in cols:
        if samples_df[c].str.contains("\.| ").any():
            raise ValueError(f"Invalid IDs found in RunsXLibs table : {c}")


def drycall(cmd, dry=True):
    """logs cmd and runs it if dry=False"""
    print(cmd)
    if not dry:
        call(cmd, shell=True)


def s3transfer_file(pattern, temp, targ, dry):
    local_files = glob.glob(pattern, recursive=True)
    if len(local_files) == 1:
        drycall(
            f"aws s3 cp {local_files[0]} {targ} --storage-class INTELLIGENT_TIERING",
            dry,
        )
    elif len(local_files) > 0:
        for f in local_files:
            drycall(f"cat {f} >> {temp}", dry)
        drycall(f"aws s3 cp {temp} {targ} --storage-class INTELLIGENT_TIERING", dry)
        drycall(f"rm {temp}", dry)
    else:
        print(f"No Matches : {pattern}")


def transfer_ont(rundir, runid, s3pre, dry, sheet_id, sheet_name, tempdir):
    """"""
    samples_df = load_sample_sheet(sheet_id, sheet_name)
    if runid[0] == "R":
        samples_df["TARG_pass"] = [
            f"{s3pre}{gdir}/{oid}.pass_{l}.{rid}.{lid}.fastq.gz"
            for oid, l, lid, rid, gdir in zip(
                samples_df["ORGANISM_ID"],
                samples_df["LABEL"],
                samples_df["LIBRARY_ID"],
                samples_df["RUN_ID"],
                samples_df["GROUP_DIR"],
            )
        ]
        samples_df["TARG_fail"] = [
            f"{s3pre}{gdir}/{oid}.fail_{l}.{rid}.{lid}.fastq.gz"
            for oid, l, lid, rid, gdir in zip(
                samples_df["ORGANISM_ID"],
                samples_df["LABEL"],
                samples_df["LIBRARY_ID"],
                samples_df["RUN_ID"],
                samples_df["GROUP_DIR"],
            )
        ]
        # handle grid based barcode labels
        samples_df.INDEX = samples_df.INDEX.apply(lambda v: ONT_grid_IDX.get(v, v))
        run_df = samples_df[samples_df.RUN_ID == runid]
        if len(run_df) == 0:
            raise ValueError(f"{runid} not found in sample sheet")
        drycall(f"mkdir {tempdir}", dry)
        if len(run_df) == 1 and len(run_df.INDEX.dropna()) == 0:
            s3transfer_file(
                f"{rundir}/**/*pass/*.fastq.gz",
                f"{tempdir}/{runid}_pass.fastq.gz",
                run_df.TARG_pass.iloc[0],
                dry,
            )
            s3transfer_file(
                f"{rundir}/**/*fail/*.fastq.gz",
                f"{tempdir}/{runid}fail.fastq.gz",
                run_df.TARG_fail.iloc[0],
                dry,
            )
        else:
            for i, tpass, tfail in zip(
                run_df.INDEX.str[2:], run_df.TARG_pass, run_df.TARG_fail
            ):
                s3transfer_file(
                    f"{rundir}/**/*pass/barcode{i}/*.fastq.gz",
                    f"{tempdir}/{runid}.barcode{i}.pass.fastq.gz",
                    tpass,
                    dry,
                )
                s3transfer_file(
                    f"{rundir}/**/*fail/barcode{i}/*.fastq.gz",
                    f"{tempdir}/{runid}.barcode{i}.fail.fastq.gz",
                    tfail,
                    dry,
                )
        drycall(f"rmdir {tempdir}", dry)
    elif runid[0] == "B":
        samples_df["TARG"] = [
            f"{s3pre}{gdir}/{oid}.{l}.{rid}.{lid}.bam"
            for oid, l, lid, rid, gdir in zip(
                samples_df["ORGANISM_ID"],
                samples_df["LABEL"],
                samples_df["LIBRARY_ID"],
                samples_df["RUN_ID"],
                samples_df["GROUP_DIR"],
            )
        ]
        samples_df.INDEX = samples_df.INDEX.apply(lambda v: ONT_grid_IDX.get(v, v))
        run_df = samples_df[samples_df.RUN_ID == runid]
        if len(run_df) == 0:
            raise ValueError(f"{runid} not found in sample sheet")
        drycall(f"mkdir {tempdir}", dry)
        if len(run_df) == 1 and len(run_df.INDEX.dropna()) == 0:
            s3transfer_file(
                f"{rundir}/**/*.bam", f"{tempdir}/{runid}.bam", run_df.TARG.iloc[0], dry
            )
        else:
            for i, target in zip(run_df.INDEX.str[2:], run_df.TARG):
                s3transfer_file(
                    f"{rundir}/**/*barcode{i}.bam",
                    f"{tempdir}/{runid}.barcode{i}.bam",
                    target,
                    dry,
                )
        drycall(f"rmdir {tempdir}", dry)
    else:
        raise ValueError(f"Error. {runid} does not have proper format.")


def transfer_ill(
    rundir, runid, s3pre, dry, sheet_id, sheet_name, tempdir, omitted_libraries=[]
):
    samples_df = load_sample_sheet(sheet_id, sheet_name)
    samples_df["TARG"] = [
        f"{s3pre}{gdir}/{oid}.{l}.{rid}.{lid}"
        for oid, l, lid, rid, gdir in zip(
            samples_df["ORGANISM_ID"],
            samples_df["LABEL"],
            samples_df["LIBRARY_ID"],
            samples_df["RUN_ID"],
            samples_df["GROUP_DIR"],
        )
    ]
    samples_df["TARG1"] = samples_df.TARG + ".R1.fastq.gz"
    samples_df["TARG2"] = samples_df.TARG + ".R2.fastq.gz"
    run_df = samples_df[samples_df.RUN_ID == runid]
    if len(run_df) == 0:
        raise ValueError(f"{runid} not found in sample sheet")
    drycall(f"mkdir {tempdir}", dry)
    for lid, t1, t2 in zip(run_df.LIBRARY_ID, run_df.TARG1, run_df.TARG2):
        if lid in omitted_libraries:
            continue
        else:
            s3transfer_file(
                f"{rundir}/**/*{lid}*R1*fastq.gz",
                f"{tempdir}/{runid}.{lid}.R1.fastq.gz",
                t1,
                dry,
            )
            s3transfer_file(
                f"{rundir}/**/*{lid}*R2*fastq.gz",
                f"{tempdir}/{runid}.{lid}.R2.fastq.gz",
                t2,
                dry,
            )
    drycall(f"rmdir {tempdir}", dry)


def transfer_pb(
    rundir,
    runid,
    s3pre: str = S3PREFIX,
    dry: bool = True,
    sheet_id: str = SHEET_ID,
    sheet_name: str = SHEET_NAME,
    tempdir=os.path.join(".", "s3_transfer_temp"),
    omitted_libraries=[],
):
    """Transfer pacbio reads"""

    samples_df = load_sample_sheet(sheet_id, sheet_name)
    samples_df["TARG"] = [
        f"{s3pre}{gdir}/{oid}.pb_{{}}{l}.{rid}.{lid}"
        for oid, l, lid, rid, gdir in zip(
            samples_df["ORGANISM_ID"],
            samples_df["LABEL"],
            samples_df["LIBRARY_ID"],
            samples_df["RUN_ID"],
            samples_df["GROUP_DIR"],
        )
    ]
    # Handle barcodes

    samples_df["TARG_BAM"] = [s.format("") + ".bam" for s in samples_df.TARG]
    samples_df["TARG_PBI"] = [s.format("") + ".bam.pbi" for s in samples_df.TARG]
    samples_df["TARG_HIFI_BAM"] = [
        s.format("hifi_") + ".default.bam" for s in samples_df.TARG
    ]
    samples_df["TARG_HIFI_PBI"] = [
        s.format("hifi_") + ".default.bam.pbi" for s in samples_df.TARG
    ]
    samples_df["TARG_META"] = [s.format("") + ".metadata" for s in samples_df.TARG]
    run_df = samples_df[samples_df.RUN_ID == runid]
    run_df = run_df[run_df["DTYPE"] == "HiFi"]
    if len(run_df) == 0:
        raise ValueError(f"{runid} HiFi not found in sample sheet")
    drycall(f"mkdir {tempdir}", dry)
    lid_context_dict = match_library_context(rundir)
    for lid, t_bam, t_pbi, t_hifi_bam, t_hifi_pbi, t_meta in zip(
        run_df.LIBRARY_ID,
        run_df.TARG_BAM,
        run_df.TARG_PBI,
        run_df.TARG_HIFI_BAM,
        run_df.TARG_HIFI_PBI,
        run_df.TARG_META,
    ):

        insert = ".default"

        if lid in omitted_libraries:
            continue

        if type(lid_context_dict[lid]) == list:
            insert = f".{lid_context_dict[lid][1]}"
            s3transfer_file(
                f"{rundir}/**/*/{lid_context_dict[lid][0]}.hifi_reads{insert}.bam",
                f"{tempdir}/{runid}.{lid}.hifi_reads.default{insert}.bam",
                t_bam,
                dry,
            )
            s3transfer_file(
                f"{rundir}/**/*/{lid_context_dict[lid][0]}.hifi_reads{insert}.bam.pbi",
                f"{tempdir}/{runid}.{lid}.reads{insert}.bam.pbi",
                t_pbi,
                dry,
            )
            for suffix in PACBIO_METADATA_SUFFIXES:
                s3transfer_file(
                    f"{rundir}/**/{lid_context_dict[lid][0]}.{suffix}",
                    f"{tempdir}/{runid}.{lid}.{suffix}",
                    f"{t_meta}/{runid}.{lid}.{suffix}",
                    dry,
                )
        else:
            s3transfer_file(
                f"{rundir}/**/*/{lid_context_dict[lid]}.hifi_reads{insert}.bam",
                f"{tempdir}/{runid}.{lid}.hifi_reads.default{insert}.bam",
                t_bam,
                dry,
            )
            s3transfer_file(
                f"{rundir}/**/*/{lid_context_dict[lid]}.hifi_reads{insert}.bam.pbi",
                f"{tempdir}/{runid}.{lid}.reads{insert}.bam.pbi",
                t_pbi,
                dry,
            )
            for suffix in PACBIO_METADATA_SUFFIXES:
                s3transfer_file(
                    f"{rundir}/**/{lid_context_dict[lid]}.{suffix}",
                    f"{tempdir}/{runid}.{lid}.{suffix}",
                    f"{t_meta}/{runid}.{lid}.{suffix}",
                    dry,
                )
    drycall(f"rmdir {tempdir}", dry)


def transfer_kinnex(
    rundir,
    runid,
    s3pre: str = S3PREFIX,
    dry: bool = True,
    sheet_id: str = SHEET_ID,
    sheet_name: str = SHEET_NAME,
    tempdir=os.path.join(".", "s3_transfer_temp"),
    omitted_libraries=[],
):
    samples_df = load_sample_sheet(sheet_id, sheet_name)
    samples_df["TARG"] = [
        f"{s3pre}{gdir}/{oid}.pb_{{}}{l}.{rid}.{lid}"
        for oid, l, lid, rid, gdir in zip(
            samples_df["ORGANISM_ID"],
            samples_df["LABEL"],
            samples_df["LIBRARY_ID"],
            samples_df["RUN_ID"],
            samples_df["GROUP_DIR"],
        )
    ]
    samples_df["TARG_BAM"] = [s.format("") + ".bam" for s in samples_df.TARG]
    samples_df["TARG_PBI"] = [s.format("") + ".bam.pbi" for s in samples_df.TARG]
    samples_df["TARG_META"] = [s.format("") + ".metadata" for s in samples_df.TARG]
    run_df = samples_df[samples_df.RUN_ID == runid]
    run_df = run_df[run_df["DTYPE"] == "ISO"]
    if len(run_df) == 0:
        raise ValueError(f"{runid} ISO not found in sample sheet")
    for lid, idx, t_bam, t_pbi, t_meta in zip(
        run_df.LIBRARY_ID,
        run_df.INDEX,
        run_df.TARG_BAM,
        run_df.TARG_PBI,
        run_df.TARG_META,
    ):

        mas = idx.split("-")[1]
        iso = idx.split("-")[0]

        if lid in omitted_libraries:
            continue
        read_path = (
            f"{rundir}/*/*/*{mas}.segmented.demux.IsoSeqX_{iso}_5p--IsoSeqX_3p.bam"
        )
        if len(glob.glob(read_path)) == 0:
            raise ValueError(f"No consensusreadset files in {rundir}")

        if len(glob.glob(read_path)) > 1:
            raise ValueError(f"No consensusreadset files in {rundir}")

        if len(glob.glob(read_path)) == 1:
            s3transfer_file(
                read_path,
                f"{tempdir}/{runid}.{lid}.{mas}.{iso}.bam",
                t_bam,
                dry,
            )
            s3transfer_file(
                f"{read_path}.pbi",
                f"{tempdir}/{runid}.{lid}.{mas}.{iso}.bam.pbi",
                t_pbi,
                dry,
            )
        else:
            raise ValueError(f"{read_path} pattern hit too many files.")
