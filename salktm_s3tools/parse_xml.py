#===============================================================================
# parse_xml.py
#===============================================================================

"""parse an XML file to extract metadata for PacBio HiFi reads
"""

# Imports ======================================================================

import untangle
import glob




# Functions ====================================================================

def retrieve_barcode_context(xml_file,num):
    a = untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata['Context']
    if "--" in untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample[num].pbsample_DNABarcodes.pbsample_DNABarcode["Name"]:
        b = untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample[num].pbsample_DNABarcodes.pbsample_DNABarcode["Name"].split("--")[0]
    else:
        b = untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample[num].pbsample_DNABarcodes.pbsample_DNABarcode["Name"]
    return [a,b]

def retrieve_context(xml_file):
    return (untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata['Context'])

def retrieve_sample_name(xml_file):
    return (untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample['Name'])

def retrieve_barcode_library_id(xml_file,num):
    return (untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample[num]['Name'])

def retrieve_library_id(xml_file):
    return (untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample['Name'])

def match_library_context(rundir):
    lib_dict = {}
    consensusreadset_xml_files = glob.glob(f'{rundir}/**/*hifi_reads.consensusreadset.xml', recursive=True)
    for xml_file in consensusreadset_xml_files:
        if (len(untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample) == 1 and 
            untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample.pbsample_DNABarcodes.pbsample_DNABarcode["Name"][:2] == "bc"):
            if "--" in untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample.pbsample_DNABarcodes.pbsample_DNABarcode["Name"]:
                bid = untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample.pbsample_DNABarcodes.pbsample_DNABarcode["Name"].split("--")[0]
                lid = retrieve_library_id(xml_file)
                lib_dict[lid] = [retrieve_context(xml_file),bid]
            else:
                bid = untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample.pbsample_DNABarcodes.pbsample_DNABarcode["Name"]
                lid = retrieve_library_id(xml_file)
                lib_dict[lid] = [retrieve_context(xml_file),bid]
        elif (len(untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample) == 1 and 
            untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample.pbsample_DNABarcodes.pbsample_DNABarcode["Name"][:2] == "bc" and
            "--" not in untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample.pbsample_DNABarcodes.pbsample_DNABarcode["Name"]):
            bid = untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample.pbsample_DNABarcodes.pbsample_DNABarcode["Name"]
            lid = retrieve_library_id(xml_file)
            lib_dict[lid] = [retrieve_context(xml_file),bid]
        elif len(untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample) >= 2:
            for num in range(len(untangle.parse(xml_file).pbds_ConsensusReadSet.pbds_DataSetMetadata.pbmeta_Collections.pbmeta_CollectionMetadata.pbmeta_WellSample.pbsample_BioSamples.pbsample_BioSample)):
                lib_dict[retrieve_barcode_library_id(xml_file,num)] = retrieve_barcode_context(xml_file,num)
        else:
            lib_dict[retrieve_library_id(xml_file)] = retrieve_context(xml_file)
    return lib_dict
