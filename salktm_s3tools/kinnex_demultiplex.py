from subprocess import Popen, PIPE, call
import pandas
import argparse
import os
import os.path
import glob
import sys

def skera_dryrun(reads,mas_barcodes,output):
    print(
        f"/data1/pacbio/smrtlink/smrtcmds/bin/skera split "\
        "--log-level INFO --log-file skera.log --alarms alarms.json -j 32 "\
        f"{reads} {mas_barcodes} {output}"
    )

def skera(reads,mas_barcodes,output):
    print(f"Running skera...\nReads: {reads}\nBarcodes: {mas_barcodes}\nOutput: {output}")
    cmd = (
        f"/data1/pacbio/smrtlink/smrtcmds/bin/skera split "\
        "--log-level INFO --log-file skera.log --alarms alarms.json -j 32 "\
        f"{reads} {mas_barcodes} {output}"
    )
    with Popen(cmd, stdout=PIPE, stderr=None, shell=True) as process:
        output = process.communicate()[0].decode("utf-8").rstrip()
        print(output)

def lima_dryrun(s_reads,iso_barcodes,output):
    print(
        f"/data1/pacbio/smrtlink/smrtcmds/bin/lima -j 32 "\
        "--log-level INFO --log-file lima-isoseq.log --isoseq "\
        "--peek-guess --overwrite-biosample-names --alarms lima_alarms.json --dump-removed "\
        f"{s_reads} {iso_barcodes} {output}"
    )

def lima(s_reads,iso_barcodes,output):
    print(f"Running lima...\nReads: {s_reads}\nBarcodes: {iso_barcodes}\nOutput: {output}")
    cmd = (
        f"/data1/pacbio/smrtlink/smrtcmds/bin/lima -j 32 "\
        "--log-level INFO --log-file lima-isoseq.log --isoseq "\
        "--peek-guess --overwrite-biosample-names --alarms lima_alarms.json --dump-removed "\
        f"{s_reads} {iso_barcodes} {output}"
    )
    with Popen(cmd, stdout=PIPE, stderr=None, shell=True) as process:
        output = process.communicate()[0].decode("utf-8").rstrip()
        print(output)

def kinnex_demultiplex_cli():
    parser = argparse.ArgumentParser(
        description=(
            "runs skera and lima to segment and demultiplex kinnex sequencing output"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-r","--reads",
        help="PacBio reads barcoded using MAS SMRTbell barcoded adapters (e.g. m84120_240222_043655_s3.hifi_reads.bcM0004.bam)",
        required=True
    )
    parser.add_argument(
        "-m","--mas_barcodes",
        help="MAS-Seq Adapter barcodes used in Kinnex run (e.g. mas8_primers.fasta)",
        default="/data1/pacbio/filetransfer/r84120_20240222_003046/1_C01/primers/mas8/mas8_primers.fasta",
        required=True
    )
    parser.add_argument(
        "-i","--iso_barcodes",
        help="Iso-Seq Adapter barcodes used in Kinnex run (e.g. IsoSeq_v2_primers_12.fasta)",
        default="/data1/pacbio/filetransfer/r84120_20240222_003046/1_C01/primers/isoseqv2/IsoSeq_v2_primers_12.fasta",
        required=True
    )
    parser.add_argument(
        "-t","--threads",
        help="Number of threads to use",
        default=32
    )
    parser.add_argument(
        "-d", "--dryrun",
        action="store_true",
        help="if true, do not execute, only show what will occur"
    )
    args = parser.parse_args()
    if(args.dryrun == True):
        skera_dryrun(
            args.reads,
            args.mas_barcodes,
            f"./{('.').join(str(args.reads).split('/')[-1].split('.')[:-1])}.segmented.bam")
        lima_dryrun(
            f"./{('.').join(str(args.reads).split('/')[-1].split('.')[:-1])}.segmented.bam",
            args.iso_barcodes,
            f"./{('.').join(str(args.reads).split('/')[-1].split('.')[:-1])}.segmented.demux.bam"
        )
    elif(args.dryrun == False):
        skera(
            args.reads,
            args.mas_barcodes,
            f"./{('.').join(str(args.reads).split('/')[-1].split('.')[:-1])}.segmented.bam"
        )
        lima(
            f"./{('.').join(str(args.reads).split('/')[-1].split('.')[:-1])}.segmented.bam",
            args.iso_barcodes,
            f"./{('.').join(str(args.reads).split('/')[-1].split('.')[:-1])}.segmented.demux.bam"
        )
        print("Done.")