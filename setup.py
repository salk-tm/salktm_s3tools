from setuptools import setup


setup(
    name="salktm_s3tools",
    version="0.3.11",
    packages=["salktm_s3tools"],
    install_requires=[
        "pandas",
        "lxml",
        "untangle"
    ],
    entry_points={
        "console_scripts": [
            f"s3transfer = salktm_s3tools.s3transfer:s3transfer_cli",
            f"test_rundir = salktm_s3tools.test_rundir:test_rundir",
            f"kinnex_demultiplex = salktm_s3tools.kinnex_demultiplex:kinnex_demultiplex_cli"
        ]
    }
)
