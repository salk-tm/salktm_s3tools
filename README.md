# salktm_s3tools

a set of scripts for doing routine aws s3 related tasks (renaming files in batch, catting and copying files, etc)

## installation
```
pip install git+https://gitlab.com/salk-tm/salktm_s3tools.git
```

## Tools

#### s3transfer
```
$ s3transfer -h
usage: s3transfer [-h] [-r RUNID] [-p {ont,ill,pb}] [-s S3PREFIX] [-d]
                  [-i SHEET_ID] [-n SHEET_NAME] [-w WKDIR]
                  rundir

transfers a set of read files from local to s3

positional arguments:
  rundir                a run directory to transfer

optional arguments:
  -h, --help            show this help message and exit
  -r RUNID, --runid RUNID
                        The run id matching your run. Defaults to 'rundir'
                        (default: None)
  -p {ont,ill,pb}, --platform {ont,ill,pb}
                        which sequencer we are running on (default: ont)
  -s S3PREFIX, --s3prefix S3PREFIX
                        s3 location to transfer reads to (default: s3://salk-
                        tm-raw/reads/)
  -d, --dryrun          if true, do not execute transfer, only show which
                        would occur (default: False)
  -i SHEET_ID, --sheet_id SHEET_ID
                        the sheet id containing runs and libraries (default:
                        1-r0PweCt4K0hQsKrc1uxjkNAZMee-PwJxkYVxPGprig)
  -n SHEET_NAME, --sheet_name SHEET_NAME
                        the name of the sheet containing runs and libraries
                        (default: RUNSxLIBRARIES)
  -w WKDIR, --wkdir WKDIR
                        A location where a working directory will be created
                        (and deleted upon exit 0) (default:
                        ./s3_transfer_temp)
```
#### test_rundir
```
$ test_rundir -h
usage: test_rundir.py [-h] rundir [rundir ...]

prints a quick report on some number of run directories and shows upload status

positional arguments:
  rundir

optional arguments:
  -h, --help  show this help message and exit
